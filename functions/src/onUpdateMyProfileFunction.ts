import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";

try {
admin.initializeApp(functions.config().firebase); } catch (e) { }

//Update users profile details in different fields when they update their profile.

export const onUpdateUser = functions
    .firestore.document('/users/{userId}')
    .onUpdate(async (change, context) => {
        const before = change.before.data();
        const after = change.after.data();

        if (before.firstName !== after.firstName ||
            before.lastName !== after.lastName ||
            before.profileImage !== after.profileImage) {
            return admin.firestore().collection('/posts/')
                .where('uid', '==', before.uid).get().then(
                    (result: {
                        size: number;
                        forEach: (arg0: (doc: any) =>
                            Promise<void>) => void;
                    }) => {
                        if (result.size > 0) {
                            result.forEach(async doc => {
                                await doc.ref.update({
                                    firstName: after.firstName,
                                    lastName: after.lastName,
                                    profileImage: after.profileImage
                                })
                            })
                        }
                        return true;
                    }
                )
        } else {
            return null;
        }
    });
