import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";

try {
    admin.initializeApp(
        functions.config().firebase
    );
} catch (e) { }
const db = admin.firestore();

const FieldValue = admin.firestore.FieldValue;
const increment = FieldValue.increment(1);
const decrement = FieldValue.increment(-1);

//Calculate the number of upvotes
export const postsUpvoteFunction = functions
    .firestore.document('posts/{postID}/upvotes/{userID}').onWrite((change, context) => {

        const postID = context.params.postID;
        console.log(postID);
        const docRef = db.collection('posts').doc(postID);

        if (!change.before.exists) {
            // New document Created : add one to count

            docRef.update({ numberOfUpVotes: increment })
                .then(() => console.log('Increment likes'))
                .catch(err => (err));

        } else if (change.before.exists && change.after.exists) {
            // Updating existing document : Do nothing

        } else if (!change.after.exists) {
            // Deleting document : subtract one from count
            docRef.get().then(function (doc) {
                if (doc.exists) {
                    if (!(doc.get('numberOfUpVotes') <= 0)) {
                        docRef.update({ numberOfUpVotes: decrement })
                            .then(() => console.log('Decrement likes '))
                            .catch(err => (err));
                    } else {
                        console.log('No Decrement')
                    }
                }
            }).then(() => console.log('Decrement likes '))
                .catch(err => (err));
        }

        return true;
    });