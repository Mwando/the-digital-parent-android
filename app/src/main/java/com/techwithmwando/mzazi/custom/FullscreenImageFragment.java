package com.techwithmwando.mzazi.custom;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.techwithmwando.mzazi.R;
import com.techwithmwando.mzazi.models.PostsModel;

public class FullscreenImageFragment extends Fragment {

    private ScalingImageView scalingImageView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fullscreen_image, container, false);

        Bundle bundle = this.getArguments();

        if (bundle != null) {
            String postImage = bundle.getString("postImage");
            scalingImageView = view.findViewById(R.id.scaling_image);
            setScalingImageView(postImage);
        }

        return view;
    }

    private void setScalingImageView(String postImage) {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.mipmap.ic_launcher);

        Glide.with(getActivity())
                .setDefaultRequestOptions(requestOptions)
                .load(postImage)
                .into(scalingImageView);
    }
}