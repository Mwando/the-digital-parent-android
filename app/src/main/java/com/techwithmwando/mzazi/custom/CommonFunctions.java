package com.techwithmwando.mzazi.custom;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class CommonFunctions {

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager
                    .hideSoftInputFromWindow(currentFocusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static boolean isConnectedToNetwork(Context context) {

        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isConnected = false;
        if (cm != null) {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            isConnected = (activeNetwork != null) && (activeNetwork.isConnected());
        }
        return isConnected;
    }


//     private boolean haveNetwork() {
//        boolean have_WIFI = false;
//        boolean have_MobileData = false;
//
//        ConnectivityManager connectivityManager = (ConnectivityManager)
//                context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo[] networkInformation = connectivityManager.getAllNetworkInfo();
//
//        for (NetworkInfo info : networkInformation) {
//            if (info.getTypeName().equalsIgnoreCase("WIFI"))
//                if (info.isConnected())
//                    have_WIFI = true;
//            if (info.getTypeName().equalsIgnoreCase("MOBILE"))
//                if (info.isConnected())
//                    have_MobileData = true;
//        }
//        return have_MobileData || have_WIFI;
}
