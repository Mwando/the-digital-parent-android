package com.techwithmwando.mzazi;

import android.annotation.SuppressLint;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.techwithmwando.mzazi.custom.CommonFunctions;
import com.techwithmwando.mzazi.posts.PostOptionsFragment;
import com.techwithmwando.mzazi.users.EditProfileActivity;
import com.techwithmwando.mzazi.users.LoginActivity;
import com.techwithmwando.mzazi.users.ProfileActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements PostOptionsFragment.BottomSheetListener {

    String currentUserID;
    private FirebaseAuth mAuth;
    private DocumentReference usersReference;
    boolean hasProfilePicture = false;

    private Toolbar toolbar;
    private CircleImageView toolbarProfilePic;

    private MaterialCardView updateProfileCard;
    private AppCompatTextView updateProfileTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbarProfilePic = findViewById(R.id.toolbar_profile_picture);
        toolbarProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileActivity();
            }
        });

        updateProfileCard = findViewById(R.id.update_profile_card);
        updateProfileTextView = findViewById(R.id.update_profile_text_view);

        checkConnectionStatus();

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            loginActivity();
        }

        bottomNavigation();
    }

    private void bottomNavigation() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav_view);
        NavController navController = Navigation.findNavController(this, R.id.fragment_container);

        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams)
                bottomNavigationView.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationViewBehaviour());

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller,
                                             @NonNull NavDestination destination,
                                             @Nullable Bundle arguments) {
                if (destination.getId() == R.id.navigation_home) {
                    getSupportActionBar().setTitle("Home");
                    toolbarProfilePic.setVisibility(View.GONE);
                } else if (destination.getId() == R.id.navigation_stories) {
                    getSupportActionBar().setTitle("Stories");
                    toolbarProfilePic.setVisibility(View.VISIBLE);
                } else if (destination.getId() == R.id.navigation_baby_products) {
                    getSupportActionBar().setTitle("Products");
                    toolbarProfilePic.setVisibility(View.VISIBLE);
                } else {
                    getSupportActionBar().setTitle("The Digital Parent");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.action_feedback_support) {
            Toast.makeText(this, "Feedback & Support", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.action_share) {
            Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.action_report_problem) {
            Toast.makeText(this, "Report a problem", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.action_logout) {
            mAuth.signOut();
            finish();
            loginActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser == null) {
            loginActivity();
        } else {
            checkProfileUpdate();
        }
    }

    private void checkConnectionStatus() {

        if (!CommonFunctions.isConnectedToNetwork(this)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    "NETWORK ERROR! \nPlease check your Internet connection",
                    Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void checkProfileUpdate() {
        final String current_uid = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        currentUserID = mAuth.getCurrentUser().getUid();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference usersReference = db.collection("users");

        usersReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<String> list = new ArrayList<>();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        list.add(document.getId());
                    }
                    if (!list.contains(current_uid)) {
                        updateProfileCard.setVisibility(View.VISIBLE);
                        toolbarProfilePic.setVisibility(View.GONE);

                        updateProfileTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editProfile();
                            }
                        });
                    } else {
                        loadToolBarProfilePicture();
                    }
                }
            }
        });
    }

    private void loadToolBarProfilePicture() {

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        usersReference = db.collection("users").document(currentUserID);

        usersReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e == null) {
                    if (documentSnapshot.exists() && !(documentSnapshot.get("profileImage") == null)) {
                        hasProfilePicture = true;
                        String myProfileImage = Objects
                                .requireNonNull(documentSnapshot.get("profileImage")).toString();

                        try {
                            Glide.with(MainActivity.this)
                                    .load(myProfileImage)
                                    .placeholder(R.drawable.ic_baseline_person_24)
                                    .into(toolbarProfilePic);
                        } catch (Exception exception) {
                            Log.d("Exception", "Error ===" + exception);

                        }
                    } else {
                        hasProfilePicture = false;
                        Snackbar snackBar = Snackbar
                                .make(findViewById(android.R.id.content),
                                        "Please update your profile picture",
                                        Snackbar.LENGTH_INDEFINITE);
                        snackBar.show();
//                        editProfile();
                    }
                }
            }
        });
    }

    private void loginActivity() {
        Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        finish();
        startActivity(loginIntent);
    }

    private void profileActivity() {
        if (hasProfilePicture) {
            Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
            finish();
            startActivity(profileIntent);
        } else {
            editProfile();
        }
    }

    private void editProfile() {
        Intent editProfileIntent = new
                Intent(MainActivity.this, EditProfileActivity.class);
        finish();
        startActivity(editProfileIntent);
    }

    @Override
    public void onBottomSheetItemClick(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}