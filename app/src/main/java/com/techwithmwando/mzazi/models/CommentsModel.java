package com.techwithmwando.mzazi.models;

import com.google.firebase.Timestamp;

public class CommentsModel {

    public String whoCommented;
    public String firstName;
    public String lastName;
    public String profileImage;
    public Timestamp timestamp;
    public int commentUpVotes;
    public String response;

    public CommentsModel(){

    }

    public CommentsModel(String whoCommented, String firstName, String lastName, String profileImage,
                         Timestamp timestamp, int commentUpVotes, String response) {
        this.whoCommented = whoCommented;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profileImage = profileImage;
        this.timestamp = timestamp;
        this.commentUpVotes = commentUpVotes;
        this.response = response;
    }

    public String getWhoCommented() {
        return whoCommented;
    }

    public void setWhoCommented(String whoCommented) {
        this.whoCommented = whoCommented;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getCommentUpVotes() {
        return commentUpVotes;
    }

    public void setCommentUpVotes(int commentUpVotes) {
        this.commentUpVotes = commentUpVotes;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
