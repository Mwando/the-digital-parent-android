package com.techwithmwando.mzazi.models;

import com.google.firebase.Timestamp;

public class PostsModel {
    public String uid;
    public String profileImage;
    public String description;
    public String firstName;
    public String lastName;
    public Timestamp timestamp;
    public String postImage;
    public int numberOfUpVotes;

    //create a public constructor
    public PostsModel() {
    }


    public PostsModel(String uid, String profileImage, String description, String firstName,
                      String lastName, Timestamp timestamp, String postImage, int numberOfUpVotes) {
        this.uid = uid;
        this.profileImage = profileImage;
        this.description = description;
        this.firstName = firstName;
        this.lastName = lastName;
        this.timestamp = timestamp;
        this.postImage = postImage;
        this.numberOfUpVotes = numberOfUpVotes;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public int getNumberOfUpVotes() {
        return numberOfUpVotes;
    }

    public void setNumberOfUpVotes(int numberOfUpVotes) {
        this.numberOfUpVotes = numberOfUpVotes;
    }
}

