package com.techwithmwando.mzazi.users;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.techwithmwando.mzazi.MainActivity;
import com.techwithmwando.mzazi.R;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private MaterialButton buttonSignIn;
    private TextInputEditText editTextEmail;
    private TextInputEditText editTextPassword;
    private AppCompatTextView textViewRegister;
    private AppCompatTextView textViewResetPassword;
    private AppCompatTextView googleSignInButton;
    private ProgressDialog progressDialog;

    private FirebaseAuth mAuth;

    private boolean emailAddressChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        editTextEmail = findViewById(R.id.login_email);
        editTextPassword = findViewById(R.id.login_password);
        buttonSignIn = findViewById(R.id.button_sign_in);
        textViewRegister = findViewById(R.id.text_view_register);
        textViewResetPassword = findViewById(R.id.text_view_forgot_password);
        googleSignInButton = findViewById(R.id.google_sign_in_button);

        progressDialog = new ProgressDialog(this);

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.loginUser();
            }
        });
        textViewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.registerUser();
            }
        });
        textViewResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.resetPassword();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        //check for current user
        if (currentUser != null) {
            mainActivity();
        }

    }

    private void loginUser() {
        String userEmail = Objects.requireNonNull(editTextEmail.getText()).toString().trim();
        String password = Objects.requireNonNull(editTextPassword.getText()).toString().trim();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

        //checking if email and password is empty
        if (userEmail.isEmpty()) {
            editTextEmail.setError("Please enter your email");
            return;
        }
        if (password.isEmpty()) {
            editTextPassword.setError("Please enter your password");
            return;
        }
        if (password.length() < 8) {
            editTextPassword.setError("Minimum password length is 8 characters");
            return;
        }
        if (password.length() > 32) {
            editTextPassword.setError("Maximum password length is 32 characters");
        }
        if (userEmail.matches((emailPattern)) || userEmail.matches(emailPattern2)) {

            //validations okay we show a progress bar
            progressDialog.setTitle(userEmail + "...");
            progressDialog.setMessage("Logging in, Please wait...");
            progressDialog.setCanceledOnTouchOutside(true);
            progressDialog.show();

            mAuth.signInWithEmailAndPassword(userEmail, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();
                            if (task.isSuccessful()) {

                                LoginActivity.this.VerifyEmailAddress();
                            } else {
                                String message = Objects
                                        .requireNonNull(task.getException()).getMessage();
                                Snackbar snackbar = Snackbar
                                        .make(LoginActivity.this.findViewById(android.R.id.content),
                                                "An Error Occurred: " + message,
                                                Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                            progressDialog.dismiss();
                        }
                    });
        } else {
            editTextEmail.setError("Please input a valid Email");
        }
    }

    private void VerifyEmailAddress() {
        FirebaseUser user = mAuth.getCurrentUser();
        assert user != null;
        emailAddressChecker = user.isEmailVerified();

        if (emailAddressChecker) {
            mainActivity();
        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                    "Please check your email and verify your email address first",
                    Snackbar.LENGTH_LONG);
            snackbar.show();
            mAuth.signOut();
        }
    }

    private void mainActivity() {
        Intent mainActivityIntent = new
                Intent(LoginActivity.this, MainActivity.class);
        finish();
        startActivity(mainActivityIntent);
    }

    private void registerUser() {
        Intent registerIntent = new
                Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    private void resetPassword() {
        Intent resetPasswordIntent = new
                Intent(LoginActivity.this, ResetPasswordActivity.class);
        startActivity(resetPasswordIntent);
    }
}