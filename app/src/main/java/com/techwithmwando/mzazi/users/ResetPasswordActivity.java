package com.techwithmwando.mzazi.users;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.techwithmwando.mzazi.R;

import java.util.Objects;

public class ResetPasswordActivity extends AppCompatActivity {

    private TextView backButton;
    private MaterialButton resetPasswordButton;
    private TextInputEditText resetEmailInput;

    private ProgressDialog progressDialog;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        mAuth = FirebaseAuth.getInstance();

        backButton = findViewById(R.id.back_button);
        resetPasswordButton = findViewById(R.id.button_send_email);
        resetEmailInput = findViewById(R.id.reset_pwd_email);

        progressDialog = new ProgressDialog(this);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginActivity();
            }
        });

        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userEmail = Objects.requireNonNull(resetEmailInput.getText()).toString();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

                if (userEmail.isEmpty()) {
                    resetEmailInput.setError("Email is required");
                }
                if (userEmail.matches((emailPattern)) || userEmail.matches(emailPattern2)) {

                    progressDialog.setTitle("Resetting Password...");
                    progressDialog.setMessage("Please wait, Operation in process...");
                    progressDialog.show();
                    progressDialog.setCanceledOnTouchOutside(true);

                    mAuth.sendPasswordResetEmail(userEmail)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Snackbar snackbar = Snackbar
                                                .make(
                                                        ResetPasswordActivity
                                                                .this
                                                                .findViewById(android.R.id.content),
                                                        "Email sent. Please check your inbox",
                                                        Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                        loginActivity();
                                    } else {
                                        String message = Objects
                                                .requireNonNull(task.getException()).getMessage();
                                        Snackbar snackbar = Snackbar
                                                .make(
                                                        ResetPasswordActivity
                                                                .this
                                                                .findViewById(android.R.id.content),
                                                        "Error: \n" + message +"\n Try Again" ,
                                                        Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }
                                    progressDialog.dismiss();
                                }
                            });
                }
                else {
                    resetEmailInput.setError("Please input a valid Email");

                }
            }
        });
    }
    private void loginActivity() {
        Intent loginIntent = new Intent(
                ResetPasswordActivity.this, LoginActivity.class);
        finish();
        startActivity(loginIntent);
    }
}
