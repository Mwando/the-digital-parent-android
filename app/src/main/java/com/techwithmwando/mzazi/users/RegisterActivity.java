package com.techwithmwando.mzazi.users;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.techwithmwando.mzazi.MainActivity;
import com.techwithmwando.mzazi.R;

import java.util.Objects;

public class RegisterActivity extends AppCompatActivity {

    private TextView backButton;
    private MaterialButton buttonRegister;
    private TextInputEditText editTextEmail;
    private TextInputEditText editTextPassword;
    private TextInputEditText editTextConfirmPassword;
    private TextView textViewSignIn;

    private ProgressDialog progressDialog;

    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(this, EditProfileActivity.class));
        }

        progressDialog = new ProgressDialog(this);

        backButton = findViewById(R.id.back_button);
        editTextEmail = findViewById(R.id.edit_text_email);
        editTextPassword = findViewById(R.id.edit_text_password);
        editTextConfirmPassword = findViewById(R.id.edit_text_confirm_password);

        buttonRegister = findViewById(R.id.button_register);
        textViewSignIn = findViewById(R.id.text_view_sign_in);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               openLoginActivity();
            }
        });
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterActivity.this.createNewAccount();
            }
        });
        textViewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterActivity.this.openLoginActivity();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        //check for current user
        if (currentUser != null) {
            mainActivity();
        }
    }

    private void createNewAccount() {
        String userEmail = Objects.requireNonNull(editTextEmail.getText()).toString().trim();
        String password = Objects.requireNonNull(editTextPassword.getText()).toString().trim();
        String confirmPassword = Objects
                .requireNonNull(editTextConfirmPassword.getText()).toString().trim();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

        //checking if email and password is empty
        if (userEmail.isEmpty()) {
            editTextEmail.setError("Please enter your email");
            return;
        }
        if (password.isEmpty()) {
            editTextPassword.setError("Please enter your password");
            return;
        }
        if (confirmPassword.isEmpty()) {
            editTextPassword.setError("Please confirm your password");
            return;
        }
        if (!password.equals(confirmPassword)) {
            editTextPassword.setError("Make sure Passwords match");
            return;
        }
        if (password.length() < 8) {

            editTextPassword.setError("Minimum password length is 8 characters");
            return;
        }
        if (password.length() > 32) {

            editTextPassword.setError("Maximum password length is 32 characters");

        }
        if (userEmail.matches((emailPattern)) || userEmail.matches(emailPattern2)) {
            progressDialog.setTitle("Registering " + userEmail + "...");
            progressDialog.setMessage("Please wait, Creating account...");
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(true);

            //create account to fireBase now
            mAuth.createUserWithEmailAndPassword(userEmail, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                Snackbar snackbar = Snackbar
                                        .make(RegisterActivity.this.findViewById(android.R.id.content),
                                                "Registration is successful, \r\n" +
                                                        " Please check your EMAIL and verify your account...",
                                                Snackbar.LENGTH_LONG);
                                snackbar.show();

                                RegisterActivity.this.emailVerificationMessage();

                            } else {
                                String message = Objects
                                        .requireNonNull(task.getException()).getMessage();
                                Snackbar snackbar = Snackbar
                                        .make(RegisterActivity
                                                        .this
                                                        .findViewById(android.R.id.content),
                                                "An Error Occurred: " + message,
                                                Snackbar.LENGTH_LONG);
                                snackbar.show();
                            }
                            progressDialog.dismiss();
                        }
                    });
        }
        else {
            editTextEmail.setError("Please input a valid Email");
        }
    }

    private void emailVerificationMessage() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if (task.isSuccessful()) {
                        RegisterActivity.this.openLoginActivity();
                    } else {
                        String message = Objects.requireNonNull(task.getException()).getMessage();
                        Snackbar snackbar = Snackbar
                                .make(RegisterActivity.this.findViewById(android.R.id.content),
                                        "Error: " + message, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    mAuth.signOut();
                }
            });
        }
    }

    private void openLoginActivity() {
        Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    private void mainActivity() {
        Intent registerIntent = new Intent(RegisterActivity.this, MainActivity.class);
        finish();
        startActivity(registerIntent);
    }

}