package com.techwithmwando.mzazi.users;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.techwithmwando.mzazi.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilePreviewFragment extends BottomSheetDialogFragment {

    private DocumentReference usersReference;
    private FirebaseFirestore db;

    private CircleImageView userProfilePicture;
    private TextView userProfileName;
    private TextView userProfileStatus;
    private MaterialButton buttonSendMessage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_preview, container, false);


        userProfilePicture = view.findViewById(R.id.user_profile_picture);
        userProfileName = view.findViewById(R.id.text_view_user_full_name);
        userProfileStatus = view.findViewById(R.id.text_view_user_status);
        buttonSendMessage = view.findViewById(R.id.button_send_message);

        Bundle bundle = getArguments();
        String postProfilePic = bundle.getString("postProfilePic");
        String postUserName = bundle.getString("postUserName");
        String postUserID = bundle.getString("postUserID");

        Glide.with(getContext())
                .load(postProfilePic)
                .placeholder(R.color.colorPrimary)
                .into(userProfilePicture);
        userProfileName.setText(postUserName);

        db = FirebaseFirestore.getInstance();
        usersReference = db.collection("users").document(postUserID);
        usersReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException error) {
                if (documentSnapshot.exists()) {
                    String status = documentSnapshot.get("status").toString();
                    userProfileStatus.setText(status);
                }
            }
        });

        buttonSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "SEND MESSAGE", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }
}