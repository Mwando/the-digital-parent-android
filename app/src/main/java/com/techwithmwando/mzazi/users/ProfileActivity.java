package com.techwithmwando.mzazi.users;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.techwithmwando.mzazi.MainActivity;
import com.techwithmwando.mzazi.R;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private CircleImageView profilePicture;
    private TextView status;
    private TextView fullName;
    private TextView userEmail;
    private TextView phoneNumber;
    private TextView dateOfBirth;
    private TextView userGender;
    private TextView numberOfChildren;
    private MaterialButton editProfileButton;

    String currentUserID;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference usersReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mAuth = FirebaseAuth.getInstance();
        currentUserID = mAuth.getCurrentUser().getUid();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setTitle("My Profile");


        db = FirebaseFirestore.getInstance();
        usersReference = db.collection("users").document(currentUserID);

        profilePicture = findViewById(R.id.image_view_profile_picture);
        status = findViewById(R.id.text_view_status);
        fullName = findViewById(R.id.text_view_full_name);
        userEmail = findViewById(R.id.text_view_email);
        phoneNumber = findViewById(R.id.text_view_phone_number);
        dateOfBirth = findViewById(R.id.text_view_DOB);
        userGender = findViewById(R.id.text_view_gender);
        numberOfChildren = findViewById(R.id.text_view_number_of_children);
        editProfileButton = findViewById(R.id.edit_profile_button);

        editProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editProfile();
            }
        });

        loadUserDetails();
    }

    private void loadUserDetails() {
        usersReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists() && documentSnapshot != null &&
                        !(documentSnapshot.get("profileImage") == null)) {

                    String myProfileImage = Objects
                            .requireNonNull(documentSnapshot.get("profileImage")).toString();
                    String myEmail = Objects
                            .requireNonNull(documentSnapshot.get("email")).toString();
                    String myStatus = Objects
                            .requireNonNull(documentSnapshot.get("status")).toString();
                    String myGender = Objects
                            .requireNonNull(documentSnapshot.get("gender")).toString();
                    String first_name = Objects
                            .requireNonNull(documentSnapshot.get("firstName")).toString();
                    String last_name = Objects
                            .requireNonNull(documentSnapshot.get("lastName")).toString();

                    String myPhoneNumber = Objects
                            .requireNonNull(documentSnapshot.get("phoneNumber")).toString();
                    String myDateOfBirth = Objects
                            .requireNonNull(documentSnapshot.get("dob")).toString();
                    String myNumberOfChildren = Objects
                            .requireNonNull(documentSnapshot.get("numberOfChildren")).toString();
                    Glide.with(ProfileActivity.this)
                            .load(myProfileImage)
                            .placeholder(R.drawable.ic_baseline_person_24)
                            .into(profilePicture);


                    status.setText(myStatus);
                    fullName.setText("Name: " + first_name + " " + last_name);
                    userEmail.setText(myEmail);
                    phoneNumber.setText("Phone Number: " + myPhoneNumber);
                    dateOfBirth.setText("Date of Birth: " + myDateOfBirth);
                    userGender.setText("Gender: " + myGender);
                    numberOfChildren.setText("Number of children: " + myNumberOfChildren);

                } else {
                    Snackbar snackBar = Snackbar
                            .make(findViewById(android.R.id.content),
                                    "Please update your profile",
                                    Snackbar.LENGTH_INDEFINITE);
                    snackBar.show();
                    editProfile();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        currentUserID = currentUser.getUid();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            mainActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void mainActivity() {
        Intent registerIntent = new
                Intent(ProfileActivity.this, MainActivity.class);
        finish();
        startActivity(registerIntent);
    }

    private void editProfile() {
        Intent editProfileIntent = new
                Intent(ProfileActivity.this, EditProfileActivity.class);
        startActivity(editProfileIntent);
    }
}