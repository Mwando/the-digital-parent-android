package com.techwithmwando.mzazi.users;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hbb20.CountryCodePicker;
import com.techwithmwando.mzazi.R;
import com.theartofdev.edmodo.cropper.CropImage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {

    private static final String TAG = "";
    String currentUserID;
    private FirebaseAuth mAuth;
    private DocumentReference usersReference;
    private StorageReference userProfileImageRef;

    private TextInputEditText editTextFirstName;
    private TextInputEditText editTextLastName;
    private TextInputEditText editTextDOB;
    private CountryCodePicker countryCodePicker;
    private TextInputEditText editTextPhoneNumber;
    private CircleImageView profileImage;
    private MaterialButton buttonSave;
    private ProgressDialog progressDialog;
    private String downloadImageUrl;
    private String countryCodeAndPhone;
    private MaterialAutoCompleteTextView selectYourGenderTextView;
    private ImageView resetSelectedGender;
    private ProgressBar progressBar;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setTitle("Update profile");

        mAuth = FirebaseAuth.getInstance();
        currentUserID = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        usersReference = db.collection("users").document(currentUserID);

        userProfileImageRef = FirebaseStorage.getInstance().getReference().child("profilePictures");

        if (mAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
        profileImage = findViewById(R.id.profile_image_view);
        editTextFirstName = findViewById(R.id.edit_text_first_name);
        editTextLastName = findViewById(R.id.edit_text_last_name);
        editTextDOB = findViewById(R.id.edit_text_DOB);
        countryCodePicker = findViewById(R.id.country_code_picker);
        editTextPhoneNumber = findViewById(R.id.edit_text_phone_number);
        selectYourGenderTextView = findViewById(R.id.edit_profile_gender_selector);
        resetSelectedGender = findViewById(R.id.refresh_gender_button);
        buttonSave = findViewById(R.id.button_save);
        progressBar = findViewById(R.id.progress_bar);

        progressDialog = new ProgressDialog(this);

        //Users country code
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getNetworkCountryIso();
        countryCodePicker.setDefaultCountryUsingNameCode(countryCode);
        countryCodePicker.setCountryPreference(countryCode);


        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //method for choosing an image file
                EditProfileActivity.this.imageFileChooser();
            }
        });

        //for gender selection
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, getResources()
                .getStringArray(R.array.gender_names));
        final String[] selection = new String[1];
        selectYourGenderTextView.setAdapter(arrayAdapter);
        selectYourGenderTextView.setCursorVisible(false);
        selectYourGenderTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectYourGenderTextView.showDropDown();

                resetSelectedGender.setAlpha(.8f);
            }
        });

        selectYourGenderTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                selectYourGenderTextView.showDropDown();
            }
        });

        resetSelectedGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectYourGenderTextView.setText(null);
                resetSelectedGender.setAlpha(.2f);
                selection[0] = null;
            }
        });


        final Calendar myCalendar = Calendar.getInstance();


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                String myFormat = "dd/MM/yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.UK);

                editTextDOB.setText(sdf.format(myCalendar.getTime()));
            }
        };

        editTextDOB.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfileActivity.this,
                            R.style.DatePickerDialogTheme,
                            date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH));

                    Date today = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(today);
                    cal.add(Calendar.YEAR, -18);// Subtract 6 months
                    long minDate = cal.getTime().getTime();
                    cal.add(Calendar.YEAR, -52);
                    long maxDate = cal.getTime().getTime();

                    datePickerDialog.getDatePicker().setMaxDate(minDate);
                    datePickerDialog.getDatePicker().setMinDate(maxDate);
//                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    datePickerDialog.show();
                }
                return true;
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show progress dialog
                progressDialog.setTitle("Profile Details");
                progressDialog.setMessage("Updating profile details, Please wait...");
                progressDialog.setCanceledOnTouchOutside(true);
                progressDialog.show();

                EditProfileActivity.this.saveUserInformation();
            }
        });

        usersReference
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                        @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot != null) {
                            if (documentSnapshot.exists()) {
                                String first_name = Objects
                                        .requireNonNull(documentSnapshot.get("firstName")).toString();
                                String last_name = Objects
                                        .requireNonNull(documentSnapshot.get("lastName")).toString();
                                String d_o_b = Objects
                                        .requireNonNull(documentSnapshot.get("dob")).toString();
                                String gender_ = Objects
                                        .requireNonNull(documentSnapshot.get("gender")).toString();
                                String phone_number = Objects
                                        .requireNonNull(documentSnapshot.get("phoneNumber")).toString();

                                //Split phone number and country code
                                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                                Phonenumber.PhoneNumber numberProto = null;
                                try {
                                    numberProto = phoneUtil.parse(phone_number, "");
                                } catch (NumberParseException ex) {
                                    ex.printStackTrace();
                                    Log.d("", "==Convert Phone error===" + ex);
                                }
                                int countryCode = numberProto.getCountryCode();
                                long nationalNumber = numberProto.getNationalNumber();
                                Log.i("code", "country code " + countryCode);
                                Log.i("code", "national number " + nationalNumber);

                                String formattedCode = Integer.toString(countryCode);
                                String formattedPhone = Long.toString(nationalNumber);

                                countryCodePicker.setFullNumber(formattedCode);
                                editTextFirstName.setText(first_name);
                                editTextLastName.setText(last_name);
                                editTextDOB.setText(d_o_b);
                                editTextPhoneNumber.setText(formattedPhone);
                                selectYourGenderTextView.setText(gender_);

                                if (documentSnapshot.exists() &&
                                        documentSnapshot.contains("profileImage") &&
                                        !(documentSnapshot.get("profileImage") == null)) {
                                    downloadImageUrl = documentSnapshot.get("profileImage").toString();
                                    Glide.with(getApplicationContext())
                                            .load(downloadImageUrl)
                                            .centerCrop()
                                            .placeholder(R.drawable.ic_baseline_person_24)
                                            .into(profileImage);
                                    progressBar.setVisibility(View.GONE);
                                } else {
                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                                            "Please select a profile picture first",
                                            Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }
                            }
                        }
                    }
                });
    }

    private void imageFileChooser() {
        CropImage.activity()
                .setMinCropResultSize(100, 100)
                .setMaxCropResultSize(5000, 5000)
                .start(EditProfileActivity.this);
    }

    /**
     * adding a profile image to fireBase storage
     * <p>
     * method for picking the chosen image from my gallery
     *
     * @param requestCode - The request code
     * @param resultCode  - Result
     * @param data        -Result data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                assert result != null;

                //show progress dialog
                progressDialog.setTitle("Profile Image");
                progressDialog.setMessage("Updating profile image, Please wait...");
                progressDialog.setCanceledOnTouchOutside(true);
                progressDialog.show();

                Uri resultUri = result.getUri();

                //Adding to storage by User ID
                final StorageReference filePath = userProfileImageRef
                        .child(resultUri
                                .getLastPathSegment() + currentUserID + ".jpg");

                //now store in fireBase storage
                final UploadTask uploadTask = filePath.putFile(resultUri);

                //Load to profile picture
                Glide.with(this).load(resultUri).into(profileImage);

                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        Task<Uri> task = uploadTask
                                .continueWithTask(
                                        new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                            @Override
                                            public Task<Uri>
                                            then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                if (!task.isSuccessful()) {
                                                    throw Objects.requireNonNull(task.getException());
                                                }
                                                //get the url
                                                downloadImageUrl = filePath.getDownloadUrl().toString();
                                                return filePath.getDownloadUrl();
                                            }
                                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {
                                            downloadImageUrl = task.getResult().toString();

                                            if (task.isSuccessful()) {
                                                Snackbar snackbar = Snackbar
                                                        .make(findViewById(android.R.id.content),
                                                                "Profile image upload successful...",
                                                                Snackbar.LENGTH_SHORT);
                                                snackbar.show();
                                            } else {
                                                String message = Objects
                                                        .requireNonNull(task.getException())
                                                        .getMessage();
                                                Snackbar snackbar = Snackbar
                                                        .make(findViewById(android.R.id.content),
                                                                "Error occurred  " + message,
                                                                Snackbar.LENGTH_LONG);
                                                snackbar.show();
                                            }
                                            progressDialog.dismiss();
                                        }
                                    }
                                });
                    }
                });
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() == null) {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    //Validation where both details must be filled and saved to fireBase database
    private void saveUserInformation() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date);
        String email = mAuth.getCurrentUser().getEmail();
        final String gender = selectYourGenderTextView.getText().toString().trim();
        String firstName = Objects.requireNonNull(editTextFirstName.getText()).toString().trim();
        String lastName = Objects.requireNonNull(editTextLastName.getText()).toString().trim();
        String dob = Objects.requireNonNull(editTextDOB.getText()).toString();
        countryCodePicker.registerCarrierNumberEditText(editTextPhoneNumber);
        countryCodeAndPhone = countryCodePicker.getFormattedFullNumber();

        if (gender.isEmpty()) {
            selectYourGenderTextView.setError("Please select your gender");
            selectYourGenderTextView.requestFocus();
            progressDialog.dismiss();
        } else if (firstName.isEmpty()) {
            editTextFirstName.setError("Required");
            editTextFirstName.requestFocus();
            progressDialog.dismiss();
        } else if (lastName.isEmpty()) {
            editTextLastName.setError("Required");
            editTextLastName.requestFocus();
            progressDialog.dismiss();
        } else if (dob.isEmpty()) {
            editTextDOB.setError("Please select your date of birth");
            progressDialog.dismiss();
        } else if (countryCodeAndPhone == null) {
            editTextPhoneNumber.setError("Please enter your phone number");
            progressDialog.dismiss();
        } else if (countryCodeAndPhone.length() < 9 || countryCodeAndPhone.length() > 15) {
            editTextPhoneNumber.setError("Enter a valid phone number");
            progressDialog.dismiss();
        } else {
            progressDialog.setTitle("Uploading Details...");
            progressDialog.setMessage("Saving your information, Please wait...");
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(true);

            final Map<String, Object> userMap = new HashMap<>();
            userMap.put("uid", currentUserID);
            userMap.put("status", "Hey there, I am using this informative MzaziApp!!!");
            userMap.put("firstName", firstName);
            userMap.put("lastName", lastName);
            userMap.put("email", email);
            userMap.put("phoneNumber", countryCodeAndPhone);
            userMap.put("dob", dob);
            userMap.put("gender", gender);
            userMap.put("numberOfChildren", "Insert");
            userMap.put("timestamp", timestamp);
            userMap.put("profileImage", downloadImageUrl);
            usersReference
                    .set(userMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Snackbar snackbar = Snackbar
                                        .make(findViewById(android.R.id.content),
                                                "Your details saved successfully",
                                                Snackbar.LENGTH_SHORT);
                                snackbar.show();
                                profileActivity();
                            } else {
                                String message = Objects.requireNonNull(task.getException()).getMessage();
                                Snackbar snackbar = Snackbar
                                        .make(findViewById(android.R.id.content),
                                                "An error occurred,please try again " + message,
                                                Snackbar.LENGTH_SHORT);
                                snackbar.show();
                            }
                            progressDialog.dismiss();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w(TAG, "Error writing document", e);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            profileActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        profileActivity();
        return true;
    }

    private void profileActivity() {
        Intent profileIntent = new
                Intent(EditProfileActivity.this, ProfileActivity.class);
        finish();
        startActivity(profileIntent);
    }
}


