package com.techwithmwando.mzazi.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techwithmwando.mzazi.R;

import de.hdodenhof.circleimageview.CircleImageView;


public class CommentsViewHolder extends RecyclerView.ViewHolder {
    public TextView commentUsersName;
    public CircleImageView commentProfileImage;
    public TextView commentPostTime;
    public TextView commentPostDate;
    public TextView postComment;

    public TextView commentPostUpVoteButton;
    public TextView commentPostShareButton;

    public CommentsViewHolder(@NonNull View itemView) {
        super(itemView);

        commentProfileImage = itemView.findViewById(R.id.response_profile_image);
        commentUsersName = itemView.findViewById(R.id.response_user_name);
        commentPostTime = itemView.findViewById(R.id.post_comment_time);
        commentPostDate = itemView.findViewById(R.id.post_comment_date);
        postComment = itemView.findViewById(R.id.post_comment);
        commentPostUpVoteButton = itemView.findViewById(R.id.post_comment_up_vote_button);

        commentPostShareButton = itemView.findViewById(R.id.text_view_comment_reply);
    }
}
