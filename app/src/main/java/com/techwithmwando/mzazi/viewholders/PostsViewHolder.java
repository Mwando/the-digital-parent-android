package com.techwithmwando.mzazi.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.RecyclerView;

import com.techwithmwando.mzazi.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostsViewHolder extends RecyclerView.ViewHolder {
    public TextView usersName;
    public CircleImageView profileImage;
    public ImageView postPicture;
    public TextView postTime;
    public TextView postDate;
    public TextView postDescription;

    //for comments
    public TextView commentOnPost;
    public TextView postTotalUpVotes;
    public TextView postTotalResponses;
    public AppCompatImageButton allPostsOptions;
    //variable for number of comments
//        int countComments;
//        DatabaseReference commentsRef;

    public TextView postUpVoteButton;
    public TextView postShareButton;

    public PostsViewHolder(@NonNull View itemView) {
        super(itemView);

        allPostsOptions = itemView.findViewById(R.id.all_posts_spinner);
        usersName = itemView.findViewById(R.id.post_user_name);
        profileImage = itemView.findViewById(R.id.post_profile_image);
        postPicture = itemView.findViewById(R.id.post_picture);
        postTime = itemView.findViewById(R.id.post_time);
        postDate = itemView.findViewById(R.id.post_date);
        postDescription = itemView.findViewById(R.id.post_query);
        postUpVoteButton = itemView.findViewById(R.id.post_up_vote_button);

        commentOnPost = itemView.findViewById(R.id.text_view_comment_reply);
        postTotalUpVotes = itemView.findViewById(R.id.post_total_up_votes);
        postTotalResponses = itemView.findViewById(R.id.post_total_responses);

        postShareButton = itemView.findViewById(R.id.text_view_share);
    }

}
