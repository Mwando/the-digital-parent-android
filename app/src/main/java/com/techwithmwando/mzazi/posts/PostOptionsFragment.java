package com.techwithmwando.mzazi.posts;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.techwithmwando.mzazi.R;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PostOptionsFragment extends BottomSheetDialogFragment {
    private BottomSheetListener bottomSheetListener;

    private FirebaseFirestore db;
    private CollectionReference postsReference;

    private TextView postActionNotifications;
    private TextView postActionEdit;
    private TextView postActionDelete;
    private TextView postActionShare;
    private TextView postActionReport;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_options, container, false);

        db = FirebaseFirestore.getInstance();
        postsReference = db.collection("posts");

        postActionNotifications = view.findViewById(R.id.post_action_notifications);
        postActionEdit = view.findViewById(R.id.post_action_edit);
        postActionDelete = view.findViewById(R.id.post_action_delete);
        postActionShare = view.findViewById(R.id.post_action_share);
        postActionReport = view.findViewById(R.id.post_action_report);

        Bundle bundle = getArguments();
        final String postID = bundle.getString("postID");
        String postUserID = bundle.getString("postUserID");
        String currentUserID = bundle.getString("currentUserID");
        Log.d("postID", postID);
        if (currentUserID.equals(postUserID)) {
            postActionEdit.setVisibility(View.VISIBLE);
            postActionDelete.setVisibility(View.VISIBLE);
        }
        if (!currentUserID.equals(postUserID)) {
            postActionNotifications.setVisibility(View.VISIBLE);
        }

        postActionNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListener.onBottomSheetItemClick("Notification on");
                dismiss();
            }
        });
        postActionEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListener.onBottomSheetItemClick("Edit post");
                dismiss();
            }
        });
        postActionDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListener.onBottomSheetItemClick("Deleted");
                deletePost(postID);
                dismiss();
            }
        });
        postActionShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListener.onBottomSheetItemClick("Share");
                dismiss();
            }
        });
        postActionReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetListener.onBottomSheetItemClick("Report problem");
                dismiss();
            }
        });

        return view;
    }

    private void deletePost(String postID) {
        postsReference.document(postID).delete();
    }

    public interface BottomSheetListener {
        void onBottomSheetItemClick(String text);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            bottomSheetListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "Must Implement BottomSheet Listener");
        }
    }
}