package com.techwithmwando.mzazi.posts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.text.LineBreaker;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.techwithmwando.mzazi.R;
import com.techwithmwando.mzazi.models.CommentsModel;
import com.techwithmwando.mzazi.viewholders.CommentsViewHolder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewPostActivity extends AppCompatActivity {
    private String postID;
    String currentUserID;

    private String firstName;
    private String lastName;
    private String profileImage;
    private String commentDetails;

    private Toolbar toolbar;
    private CircleImageView toolbarProfilePic;

    private FirebaseAuth mAuth;
    private DocumentReference postsReference;
    private CollectionReference usersReference;
    private FirebaseFirestore db;
//    private DocumentReference userRef;

    private RecyclerView commentsList;
    private ProgressBar commentsProgressBar;
    private TextView usersName;
    private CircleImageView postProfileImage;
    private ImageView postPicture;
    private TextView postTime;
    private TextView postDate;
    private TextView postDescription;
    private TextView postTotalUpVotes;
    private TextView postTotalResponses;

    private TextView postUpVoteButton;
    private TextView postApprove;

    private TextInputEditText commentOnPostTextInput;
    private AppCompatImageButton commentOnPostButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        postID = getIntent().getExtras().get("postID").toString();

        toolbarProfilePic = findViewById(R.id.toolbar_profile_picture);
        toolbarProfilePic.setVisibility(View.GONE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Query Responses");

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        db = FirebaseFirestore.getInstance();
        currentUserID = currentUser.getUid();
//        userRef = db.collection("users").document(currentUserID);
        postsReference = db.collection("posts").document(postID);
        usersReference = db.collection("users");

        commentsProgressBar = findViewById(R.id.comments_progress_bar);
        usersName = findViewById(R.id.post_user_name);
        postProfileImage = findViewById(R.id.post_profile_image);
        postPicture = findViewById(R.id.post_picture);
        postTime = findViewById(R.id.post_time);
        postDate = findViewById(R.id.post_date);
        postDescription = findViewById(R.id.post_query);
        postUpVoteButton = findViewById(R.id.post_up_vote_button);

        postTotalUpVotes = findViewById(R.id.post_total_up_votes);
        postTotalResponses = findViewById(R.id.post_total_responses);

        postApprove = findViewById(R.id.text_view_approve);
        postApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ViewPostActivity.this, "Approve", Toast.LENGTH_SHORT).show();
            }
        });

        commentOnPostTextInput = findViewById(R.id.post_comment_text_input);
        commentOnPostButton = findViewById(R.id.post_comment_button);
        commentOnPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentValidation();
            }
        });

        setPostDetails();

        commentsList = findViewById(R.id.comments_recycler);
        commentsList.setHasFixedSize(true);
        loadComments();
    }

    private void loadComments() {
        Query query = db
                .collection("posts")
                .document(postID)
                .collection("responses")
                .orderBy("timestamp", Query.Direction.ASCENDING);

        LinearLayoutManager linearLayoutManager = new
                LinearLayoutManager(this);
        commentsList.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        final FirestoreRecyclerOptions<CommentsModel> options = new
                FirestoreRecyclerOptions.Builder<CommentsModel>()
                .setQuery(query, CommentsModel.class)
                .build();

        FirestoreRecyclerAdapter<CommentsModel, CommentsViewHolder> adapter = new
                FirestoreRecyclerAdapter<CommentsModel, CommentsViewHolder>(options) {
                    @RequiresApi(api = Build.VERSION_CODES.Q)
                    @Override
                    protected void onBindViewHolder(@NonNull CommentsViewHolder holder,
                                                    int position, @NonNull CommentsModel model) {
                        commentsProgressBar.setVisibility(View.GONE);

//                         String commentID = getSnapshots().getSnapshot(position).getId();
//                         String commentUserID = model.whoCommented;
                        String firstName = model.firstName;
                        String lastName = model.lastName;
                        String postPicture = model.profileImage;
                        String postComment = model.response;
//                         int commentUpVotes = model.commentUpVotes;

                        Timestamp timestamp = model.timestamp;
                        Date date = timestamp.toDate();
                        SimpleDateFormat postDateFormat = new
                                SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault());
                        SimpleDateFormat postTimeFormat = new
                                SimpleDateFormat("kk:mm", Locale.getDefault());
                        String post_date = postDateFormat.format(date);
                        String post_time = postTimeFormat.format(date);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            holder.postComment
                                    .setJustificationMode(LineBreaker.JUSTIFICATION_MODE_INTER_WORD);
                        }

                        holder.commentUsersName.setText(String.format("%s %s", firstName, lastName));
                        Glide.with(getApplicationContext())
                                .load(postPicture)
                                .into(holder.commentProfileImage);
                        holder.commentPostTime.setText(post_time);
                        holder.commentPostDate.setText(post_date);
                        holder.postComment.setText(postComment);
//                        holder. commentPostUpVoteButton;
//                        holder. commentPostShareButton;

                    }

                    @NonNull
                    @Override
                    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                 int viewType) {

                        View view = LayoutInflater
                                .from(parent.getContext())
                                .inflate(R.layout.all_comments_layout, parent, false);
                        return new CommentsViewHolder(view);
                    }

                    @Override
                    public void onError(@NonNull FirebaseFirestoreException e) {
                        commentsProgressBar.setVisibility(View.GONE);
                        Toast.makeText(ViewPostActivity.this,
                                "Error: " + e, Toast.LENGTH_SHORT).show();
                        Log.e("error", e.getMessage());
                    }
                };

        commentsList.setLayoutManager(linearLayoutManager);
        commentsList.setAdapter(adapter);
        adapter.startListening();

    }

    private void commentValidation() {
        commentDetails = commentOnPostTextInput.getText().toString();

        if (TextUtils.isEmpty(commentDetails)) {
            Snackbar snackbar = Snackbar
                    .make(findViewById(android.R.id.content),
                            "Cannot be empty", Snackbar.LENGTH_SHORT);
            snackbar.show();
        } else {
            commentOnPost(commentDetails);
        }
    }

    private void commentOnPost(final String commentDetails) {
        usersReference.document(currentUserID)
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                        @Nullable FirebaseFirestoreException error) {
                        if (documentSnapshot.exists()) {
                            Date date = new Date();
                            Timestamp timestamp = new Timestamp(date);

                            firstName = documentSnapshot.getString("firstName");
                            lastName = documentSnapshot.getString("lastName");
                            profileImage = documentSnapshot.getString("profileImage");

                            String commentID = "comment_" + currentUserID + "_" + timestamp;

                            HashMap<String, Object> commentMap = new HashMap<>();
                            commentMap.put("whoCommented", currentUserID);
                            commentMap.put("firstName", firstName);
                            commentMap.put("lastName", lastName);
                            commentMap.put("response", commentDetails);
                            commentMap.put("profileImage", profileImage);
                            commentMap.put("timestamp", timestamp);
                            commentMap.put("commentUpVotes", 0);

                            postsReference.collection("responses").document(commentID)
                                    .set(commentMap)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(ViewPostActivity.this,
                                                    "Commented", Toast.LENGTH_SHORT).show();

                                            commentOnPostTextInput.setText(null);
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(ViewPostActivity.this,
                                            "Could not comment \n" + "Error:" + e,
                                            Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                    }
                });
    }

    private void setPostDetails() {
        postsReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException error) {
                if (error == null) {
                    if (documentSnapshot != null) {
//                        String postUID = documentSnapshot.get("uid").toString();
                        String profileImage = documentSnapshot.get("profileImage").toString();
                        String description = documentSnapshot.get("description").toString();
                        String firstName = documentSnapshot.get("firstName").toString();
                        String lastName = documentSnapshot.get("lastName").toString();
                        String noOfUpVotesStr = documentSnapshot.get("numberOfUpVotes").toString();
                        int numberOfUpVotes = Integer.parseInt(noOfUpVotesStr);

                        usersName.setText(String.format("%s %s", firstName, lastName));

                        postDescription.setText(description);
                        postTotalResponses.setText("0");

                        Timestamp timestamp = documentSnapshot.getTimestamp("timestamp");
                        Date date = timestamp.toDate();
                        SimpleDateFormat postDateFormat = new
                                SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault());
                        SimpleDateFormat postTimeFormat = new
                                SimpleDateFormat("kk:mm", Locale.getDefault());
                        String post_date = postDateFormat.format(date);
                        String post_time = postTimeFormat.format(date);
                        postTime.setText(post_time);
                        postDate.setText(post_date);

                        postUpVoteButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(ViewPostActivity.this,
                                        "Upvote", Toast.LENGTH_SHORT).show();
                            }
                        });

                        Glide.with(getApplicationContext())
                                .load(profileImage)
                                .placeholder(R.mipmap.ic_launcher)
                                .into(postProfileImage);
                        if (!(documentSnapshot.get("postImage") == null)) {
                            String postImage = documentSnapshot.get("postImage").toString();

                            Glide.with(getApplicationContext())
                                    .load(postImage)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .into(postPicture);
                            postPicture.setVisibility(View.VISIBLE);
                        } else {
                            postPicture.setVisibility(View.GONE);
                        }

                        if (numberOfUpVotes != 1) {
                            postTotalUpVotes.setText(new
                                    StringBuilder()
                                    .append(numberOfUpVotes).append(" Upvotes").toString());
                        } else {
                            postTotalUpVotes.setText(new
                                    StringBuilder()
                                    .append(numberOfUpVotes).append(" Upvote").toString());
                        }

                    }
                } else {
                    Toast.makeText(ViewPostActivity.this,
                            "Error " + error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}