package com.techwithmwando.mzazi.posts;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.text.LineBreaker;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.techwithmwando.mzazi.viewholders.PostsViewHolder;
import com.techwithmwando.mzazi.custom.CommonFunctions;
import com.techwithmwando.mzazi.R;
import com.techwithmwando.mzazi.models.PostsModel;
import com.techwithmwando.mzazi.users.ProfileActivity;
import com.techwithmwando.mzazi.users.ProfilePreviewFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeFragment extends Fragment {
    String currentUserID;
    boolean isImageFitToScreen;

    private CircleImageView writeQuestionPic;
    private MaterialCardView materialCardView;
    private RecyclerView postList;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    private FirebaseAuth mAuth;
    private DocumentReference userRef;
    private CollectionReference postsReference;
    private FirebaseFirestore db;

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        postsReference = db.collection("posts");

        postList = rootView.findViewById(R.id.all_users_query_list);
        postList.setHasFixedSize(true);

        progressBar = rootView.findViewById(R.id.news_feed_progress_bar);
        swipeRefreshLayout = rootView.findViewById(R.id.swipeToRefresh);
        materialCardView = rootView.findViewById(R.id.create_post_view);
        materialCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPostActivity();
            }
        });
        writeQuestionPic = rootView.findViewById(R.id.write_question_pic);
        writeQuestionPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profileActivity();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        HomeFragment.this.checkConnectionStatus();
                    }
                }, 1000);

                HomeFragment.this.DisplayAllUsersQueries();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        DisplayAllUsersQueries();

        return rootView;
    }

    private void loadUserProfilePic() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        currentUserID = currentUser.getUid();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        userRef = db.collection("users").document(currentUserID);

        userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e == null) {
                    if (documentSnapshot.exists()
                            && !(documentSnapshot.get("profileImage") == null)) {
                        String myProfileImage = Objects
                                .requireNonNull(documentSnapshot.get("profileImage")).toString();

                        if (getContext() != null) {
                            Glide.with(getActivity())
                                    .load(myProfileImage)
                                    .placeholder(R.drawable.ic_baseline_person_24)
                                    .into(writeQuestionPic);
                        }
                    } else {
                        Log.d("Debug", "===no=pic==");
                    }
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            currentUserID = currentUser.getUid();
            loadUserProfilePic();
        }
    }

    private void checkConnectionStatus() {
        if (!CommonFunctions.isConnectedToNetwork(getContext())) {
            Snackbar snackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                    "NETWORK ERROR! \nPlease check your Internet connection",
                    Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void DisplayAllUsersQueries() {

        Query query = db
                .collection("posts")
                .orderBy("timestamp", Query.Direction.ASCENDING);
        LinearLayoutManager linearLayoutManager = new
                LinearLayoutManager(getContext());
        postList.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        final FirestoreRecyclerOptions<PostsModel> options = new
                FirestoreRecyclerOptions.Builder<PostsModel>()
                .setQuery(query, PostsModel.class)
                .build();

        FirestoreRecyclerAdapter<PostsModel, PostsViewHolder> adapter = new
                FirestoreRecyclerAdapter<PostsModel, PostsViewHolder>(options) {
                    @RequiresApi(api = Build.VERSION_CODES.Q)
                    @Override
                    protected void onBindViewHolder(
                            @NonNull final PostsViewHolder holder, int position, @NonNull PostsModel model) {
                        progressBar.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            holder.postDescription
                                    .setJustificationMode(LineBreaker.JUSTIFICATION_MODE_INTER_WORD);
                        }

                        model = getItem(position);
                        final String postID = getSnapshots().getSnapshot(position).getId();
                        final String postUserID = model.uid;
                        final String postPicture = model.postImage;
                        final String users_name = model.firstName + " " + model.lastName;
                        final String post_profile_pic = model.profileImage;

                        Timestamp timestamp = model.timestamp;
                        Date date = timestamp.toDate();
                        SimpleDateFormat postDateFormat = new
                                SimpleDateFormat("dd-MMMM-yyyy", Locale.getDefault());
                        SimpleDateFormat postTimeFormat = new
                                SimpleDateFormat("kk:mm", Locale.getDefault());
                        String post_date = postDateFormat.format(date);
                        String post_time = postTimeFormat.format(date);

                        holder.postTime.setText(post_time);
                        holder.postDate.setText(post_date);
                        holder.postDescription.setText(model.description);
                        holder.usersName.setText(users_name);

                        Glide.with(getContext())
                                .load(post_profile_pic)
                                .placeholder(new ColorDrawable(Color.BLACK))
                                .into(holder.profileImage);

                        holder.usersName.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ProfilePreviewFragment usersDialogFragment = new
                                        ProfilePreviewFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("postProfilePic", post_profile_pic);
                                bundle.putString("postUserID", postUserID);
                                bundle.putString("postUserName", users_name);
                                usersDialogFragment.setArguments(bundle);
                                usersDialogFragment.show(getChildFragmentManager(),
                                        "usersDialogFragment");
                            }
                        });

                        holder.profileImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ProfilePreviewFragment usersDialogFragment = new
                                        ProfilePreviewFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("postUserID", postUserID);
                                bundle.putString("postUserName", users_name);
                                bundle.putString("postProfilePic", post_profile_pic);
                                usersDialogFragment.setArguments(bundle);
                                usersDialogFragment.show(getChildFragmentManager(),
                                        "usersDialogFragment");
                            }
                        });

                        if (postPicture != null) {
                            Glide.with(getContext())
                                    .load(postPicture)
                                    .placeholder(R.mipmap.ic_launcher)
                                    .into(holder.postPicture);
                            holder.postPicture.setVisibility(View.VISIBLE);
                            holder.postPicture.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    NavController navController = Navigation
                                            .findNavController(getActivity(),
                                                    R.id.fragment_container);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("postImage", postPicture);
                                    navController
                                            .navigate(R.id.action_navigation_home_to_navigation_products_image, bundle);
                                }
                            });
                        } else {
                            holder.postPicture.setVisibility(View.GONE);
                        }

                        holder.allPostsOptions.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Bottom Sheet options
                                PostOptionsFragment postsDialogFragment = new PostOptionsFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("postID", postID);
                                bundle.putString("postUserID", postUserID);
                                bundle.putString("currentUserID", currentUserID);
                                postsDialogFragment.setArguments(bundle);
                                postsDialogFragment.show(getChildFragmentManager(),
                                        "postsBottomSheet");
                            }
                        });

                        FirebaseFirestore rootRef = FirebaseFirestore.getInstance();
                        DocumentReference upVoteReference = rootRef
                                .collection("posts")
                                .document(postID)
                                .collection("upvotes")
                                .document(currentUserID);
                        //Update button based on upVote of current user
                        upVoteReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot value,
                                                @Nullable FirebaseFirestoreException error) {
                                if (error == null) {
                                    if (value.exists()) {
                                        holder.postUpVoteButton.setEnabled(false);
                                        holder.postUpVoteButton
                                                .setCompoundDrawablesWithIntrinsicBounds(
                                                        0,
                                                        R.drawable.ic_angle_double_up_primary,
                                                        0,
                                                        0);
                                        holder.postUpVoteButton
                                                .setTextColor(getResources().getColor(R.color.colorPrimary));
                                    }
                                }
                            }
                        });

                        holder.postUpVoteButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Date date = new Date();
                                Timestamp timestamp = new Timestamp(date);
                                final Map<String, Object> upVoteMap = new HashMap<>();
                                upVoteMap.put("timestamp", timestamp);
                                upVoteMap.put("uid", currentUserID);

                                postsReference.document(postID)
                                        .collection("upvotes")
                                        .document(currentUserID).set(upVoteMap)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                            }
                                        });

                            }
                        });
                        int upVotes = model.numberOfUpVotes;
                        if (upVotes != 1) {
                            holder.postTotalUpVotes.setText(String.format("%d Upvotes", upVotes));
                        } else {
                            holder.postTotalUpVotes.setText(String.format("%d Upvote", upVotes));
                        }

                        holder.postTotalResponses.setText("0 Responses");
                        holder.commentOnPost.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Bottom Sheet options
                                CommentOnPostFragment commentOnPostFragment = new CommentOnPostFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("postID", postID);
                                bundle.putString("currentUserID", currentUserID);
                                commentOnPostFragment.setArguments(bundle);
                                commentOnPostFragment.show(getChildFragmentManager(), "commentOnPostFragment");
                            }
                        });
                        holder.postShareButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(getActivity(), "Share Post", Toast.LENGTH_SHORT).show();
                            }
                        });

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent viewPostActivity = new Intent(getContext(), ViewPostActivity.class);
                                viewPostActivity.putExtra("postID", postID);
                                startActivity(viewPostActivity);
                            }
                        });

                    }

                    @NonNull
                    @Override
                    public PostsViewHolder onCreateViewHolder(
                            @NonNull ViewGroup parent, int viewType) {
                        View view = LayoutInflater
                                .from(parent.getContext())
                                .inflate(R.layout.all_posts_layout, parent, false);
                        return new PostsViewHolder(view);
                    }

                    @Override
                    public void onError(FirebaseFirestoreException e) {
                        progressBar.setVisibility(View.GONE);
                        Log.e("error", e.getMessage());
                    }
                };

        postList.setLayoutManager(linearLayoutManager);
        postList.setAdapter(adapter);
        adapter.startListening();
    }


    private void profileActivity() {
        Intent profileIntent = new Intent(getContext(), ProfileActivity.class);
        startActivity(profileIntent);
    }

    private void createPostActivity() {
        Intent profileIntent = new Intent(getContext(), CreatePostActivity.class);
        startActivity(profileIntent);
    }

}

