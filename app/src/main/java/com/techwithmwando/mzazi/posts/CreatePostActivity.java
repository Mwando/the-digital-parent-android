package com.techwithmwando.mzazi.posts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.text.LineBreaker;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.techwithmwando.mzazi.MainActivity;
import com.techwithmwando.mzazi.R;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class CreatePostActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;

    private FirebaseAuth mAuth;
    private CollectionReference usersReference;
    private CollectionReference postsReference;
    private DocumentReference userRef;
    private StorageReference postImageStorageRef;

    private TextView buttonCreatePost;
    private TextView createPostUsersName;
    private CircleImageView profile_image;
    private TextView createPostTextView;
    private TextInputEditText createPostEditText;
    private ImageButton addPostImage;
    private ImageView imageViewUpload;

    private String postDetails;
    private String postRandomName;
    private String currentUserID;
    private String firstName;
    private String lastName;
    private String profileImage;
    private String downloadImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);

        mAuth = FirebaseAuth.getInstance();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        usersReference = db.collection("users");
        postsReference = db.collection("posts");
        postImageStorageRef = FirebaseStorage.getInstance().getReference().child("usersPostImages");

        createPostTextView = findViewById(R.id.create_post_text_view);
        createPostEditText = findViewById(R.id.create_post_edit_text);
        buttonCreatePost = findViewById(R.id.button_post);
        progressDialog = new ProgressDialog(this);
        createPostUsersName = findViewById(R.id.post_user_name);
        profile_image = findViewById(R.id.create_post_profile_image);
        addPostImage = findViewById(R.id.add_post_image);
        imageViewUpload = findViewById(R.id.image_view_upload);

        createPostEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @RequiresApi(api = Build.VERSION_CODES.Q)
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                createPostTextView.setText(createPostEditText.getText().toString());
                createPostTextView.setVisibility(View.VISIBLE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createPostTextView.setJustificationMode(LineBreaker.JUSTIFICATION_MODE_INTER_WORD);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        addPostImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageFileChooser();
            }
        });

        buttonCreatePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postFormValidation();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        currentUserID = currentUser.getUid();

        if (currentUser != null) {
            currentUserID = currentUser.getUid();
            loadUserProfileDetails();
        }

    }

    private void loadUserProfileDetails() {
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        currentUserID = currentUser.getUid();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        userRef = db.collection("users").document(currentUserID);
        userRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                Log.d("Debug", "=== " + e);
                if (documentSnapshot.exists()
                        && !(documentSnapshot.get("profileImage") == null)) {
                    String myProfileImage = Objects
                            .requireNonNull(documentSnapshot.get("profileImage")).toString();
                    String myFirstName = Objects
                            .requireNonNull(documentSnapshot.get("firstName")).toString();
                    String myLastName = Objects
                            .requireNonNull(documentSnapshot.get("lastName")).toString();

                    Glide.with(CreatePostActivity.this)
                            .load(myProfileImage)
                            .placeholder(R.mipmap.ic_launcher)
                            .into(profile_image);

                    createPostUsersName.setText(myFirstName + " " + myLastName);
                } else {
                    Log.d("Debug", "===no=pic==");
                }
            }
        });
    }

    private void postFormValidation() {
        postDetails = createPostEditText.getText().toString();

        if (TextUtils.isEmpty(postDetails)) {
            Snackbar snackbar = Snackbar
                    .make(findViewById(android.R.id.content),
                            "Please ask a question", Snackbar.LENGTH_SHORT);
            snackbar.show();
        } else {
            progressDialog.setMessage("Posting, Please wait...");
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(true);

            savePostToFirestore();
        }
    }

    private void savePostToFirestore() {
        usersReference.document(currentUserID).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    Date date = new Date();
                    Timestamp timestamp = new Timestamp(date);

                    postRandomName = currentUserID + "_" + timestamp;

                    firstName = documentSnapshot.getString("firstName");
                    lastName = documentSnapshot.getString("lastName");
                    profileImage = documentSnapshot.getString("profileImage");

                    HashMap<String, Object> postMap = new HashMap<>();
                    postMap.put("uid", currentUserID);
                    postMap.put("firstName", firstName);
                    postMap.put("lastName", lastName);
                    postMap.put("profileImage", profileImage);
                    postMap.put("description", postDetails);
                    postMap.put("timestamp", timestamp);
                    postMap.put("postImage", downloadImageUrl);
                    postMap.put("numberOfUpVotes", 0);
                    postMap.put("numberOfComments", 0);

                    postsReference
                            .document(postRandomName)
                            .set(postMap)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    mainActivity();
                                    Snackbar snackbar = Snackbar
                                            .make(findViewById(android.R.id.content),
                                                    "Posted",
                                                    Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                    progressDialog.dismiss();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Snackbar snackbar = Snackbar
                                    .make(findViewById(android.R.id.content),
                                            "Please try again. An error occurred",
                                            Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            progressDialog.dismiss();
                        }
                    });
                }
            }
        });
    }

    private void imageFileChooser() {
        CropImage.activity()
                .setMinCropResultSize(100, 100)
                .setMaxCropResultSize(5000, 5000)
                .start(CreatePostActivity.this);
    }

    /**
     * adding a profile image to fireBase storage
     * <p>
     * method for picking the chosen image from my gallery
     *
     * @param requestCode - The request code
     * @param resultCode  - Result
     * @param data        -Result data
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                assert result != null;

                //show progress dialog
                progressDialog.setMessage("Saving picture, please wait...");
                progressDialog.setCanceledOnTouchOutside(true);
                progressDialog.show();

                Uri resultUri = result.getUri();

                //Adding to storage by User ID
                final StorageReference filePath = postImageStorageRef
                        .child(resultUri
                                .getLastPathSegment() + currentUserID + ".jpg");

                //now store in fireBase storage
                final UploadTask uploadTask = filePath.putFile(resultUri);

                //Load to profile picture
                Glide.with(this).load(resultUri).into(imageViewUpload);
                imageViewUpload.setVisibility(View.VISIBLE);

                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> task = uploadTask
                                .continueWithTask(new Continuation<UploadTask.TaskSnapshot,
                                        Task<Uri>>() {
                                    @Override
                                    public Task<Uri>
                                    then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                        if (!task.isSuccessful()) {
                                            throw Objects.requireNonNull(task.getException());
                                        }
                                        //get the url
                                        downloadImageUrl = filePath.getDownloadUrl().toString();
                                        return filePath.getDownloadUrl();
                                    }
                                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {
                                            downloadImageUrl = task.getResult().toString();

                                            if (task.isSuccessful()) {
                                                Snackbar snackbar = Snackbar
                                                        .make(findViewById(android.R.id.content),
                                                                "Photo saved",
                                                                Snackbar.LENGTH_SHORT);
                                                snackbar.show();
                                            } else {
                                                String message = Objects
                                                        .requireNonNull(task.getException())
                                                        .getMessage();
                                                Snackbar snackbar = Snackbar
                                                        .make(findViewById(android.R.id.content),
                                                                "Error occurred  \n" + message,
                                                                Snackbar.LENGTH_LONG);
                                                snackbar.show();
                                            }
                                            progressDialog.dismiss();
                                        }
                                    }
                                });
                    }
                });
            }
        }
    }

    private void mainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class);
        startActivity(mainActivityIntent);
    }
}

